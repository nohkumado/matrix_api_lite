import 'package:matrix_api_lite/matrix_api_lite.dart';
import 'package:matrix_api_lite/src/matrix_api.dart';
import 'package:args/args.dart';

ArgResults argResults;


void main(List<String> arguments) async
{
  var parser = ArgParser();
  parser.addOption('server', abbr: 's', defaultsTo: 'https://matrix.org');
  parser.addOption('user', abbr: 'u', defaultsTo: 'testbot');
  parser.addOption('passwd', abbr: 'p', defaultsTo: 'nuthing');
  parser.addOption('room', abbr: 'r', defaultsTo: '#testchan');
  parser.addOption('roomid', abbr: 'i');
  parser.addOption('invite', abbr: 'c');
  parser.addOption('msg', abbr: 'm', defaultsTo: 'Hello!');
  var results = parser.parse(arguments);
  print("connecting to server ${results["server"]}");
  final api = MatrixApi(homeserver: Uri.parse(results["server"]));
  print("logging in with  ${results["user"]} and ${results["passwd"]}");

  LoginResponse loginData = await api.login(user: results["user"], password: results["passwd"]);
  if(loginData != null) {
    //userid = loginData.userId;
    print("logged into server! token= ${loginData.accessToken}");
    api.accessToken = loginData.accessToken; //why on earth????
    final capabilities = await api.requestServerCapabilities();
    print(capabilities.toJson());
    print("trying to join room= ${results["room"]} and inviting ${results["invite"]}");
    String rid = await api.createRoom( roomAliasName: results["room"],  name: "a Room", topic: "testing",invite: [results["invite"]]);
    //String rid = await api.joinRoomOrAlias( roomAlias);
    print("joined room $rid!");
    Map<String,dynamic> content = {
      "msgtype": "m.text",
      "body": results["msg"]
    };

    print("trying to send  ${results["msg"]}!");
    int txnId = 1;
    String eventid = await api.sendMessage(rid, "m.room.message", "$txnId", content);
    print("send message $eventid!");
  }
  else print("failed to log in");

}
