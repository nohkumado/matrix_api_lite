## 0.1.9
- feat: Add support for fallback keys

## 0.1.8

- fix: Wrong parameters use

## 0.1.7

- change: Less noisy one-line logs

## 0.1.6

- fix: well-known in fake_matrix_api

## 0.1.5

- Add m.dummy event
- fix: Deep-copy arrays correctly

## 0.1.4

- Fix creating shallow copies all the time

## 0.1.3

- Set Content-Length on upload

## 0.1.2

- Add more encryption and decryption content parsing

## 0.1.1

- Add RoomEncryptedContent and RoomEncryptionContent

## 0.1.0

- Initial version
